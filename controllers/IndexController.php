<?php
/**
 * Created by PhpStorm.
 * User: mallmann
 * Date: 08/04/18
 * Time: 16:50
 */

namespace controllers;

use core\Controller;

class IndexController extends Controller
{
    public function index()
    {
        $this->loadTemplate('index/index');
    }

}