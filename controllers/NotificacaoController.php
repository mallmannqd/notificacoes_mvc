<?php
/**
 * Created by PhpStorm.
 * User: mallmann
 * Date: 29/04/18
 * Time: 00:27
 */

namespace controllers;


use core\Controller;
use models\Notificacao;

class NotificacaoController extends Controller
{
    public function verificar(): void
    {
        $notificacao = new Notificacao();
        $notificacoes = $notificacao->buscaNotificacoesUsuario(1);

        echo json_encode($notificacoes);
    }

    public function add(): void
    {
        $notificacao = new Notificacao();
        $notificacao->add();
    }

    public function mostraNotificacoes(): void
    {
        $notificacao = new Notificacao();
        $notificacao->mostra(1);
    }

}