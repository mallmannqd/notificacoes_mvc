function verificaNotificaco() {
    $.ajax({
        url: 'notificacao/verificar',
        type: 'POST',
        dataType: 'json',
        success:function (data) {

            console.log(data);

            if (data.qt > 0){

                $('.notificacoes').addClass('tem_notif');
                $('.notificacoes').html(data.qt);

            }else {

                $('.notificacoes').removeClass('tem_notif');
                $('.notificacoes').html('0');

            }
        }
    })
}

$(function () {
    setInterval(verificaNotificaco, 5000);

    $('#addNotif').on('click', function () {
       $.ajax({
           url: 'notificacao/add'
       });
    });

    $('.notificacoes').on('click', function () {
        $.ajax({
            url: 'notificacao/mostraNotificacoes',
            type: 'POST',
            dataType: 'html',
            success: function (html) {
                $('#textoNotificacoes').html(html);
            }
        });
    })
});