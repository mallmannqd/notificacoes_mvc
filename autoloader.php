<?php
/**
 * Created by PhpStorm.
 * User: mallmann
 * Date: 08/04/18
 * Time: 15:34
 */
spl_autoload_register("autoload");

function autoload($class){
    $class = str_replace("\\", "/", $class);
    require_once $class . '.php';
}